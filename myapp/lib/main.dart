import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
    home: Homepage()
  ));

class Homepage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
        appBar: AppBar(
          title: Text('Babbiere'),
        ),
        body: Container(
          margin: const EdgeInsets.all(10.0),
            decoration: BoxDecoration(
              //shape: BoxShape.circle,
              borderRadius: BorderRadius.all(Radius.circular(16)),
              color: Colors.white,
            ),
          child: GridView.count(
            // Create a grid with 2 columns. If you change the scrollDirection to
            // horizontal, this produces 2 rows.
            crossAxisCount: 4,
            // Generate 100 widgets that display their index in the List.
            children: List.generate(5, (index) {
              return Center(
                child: Text(
                  '$index',
                  style: Theme.of(context).textTheme.headline5,
                ),
              );
            }),
          ),
        )
    );
  }
}


class HomeGuida extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('my nigga app'),
        centerTitle: true,
        backgroundColor: Colors.brown,
      ),
      body: Center(
        /*
        child: Image.asset ('assets/bf.jpg'),
        child: Image.network ('assets/bf.jpg'),
        child: Icon(
          Icons.piano_sharp, color: Colors.amber, size: 50,
        ),
        */
         child: IconButton(
           onPressed: () {
             print('a');
           },
           icon: Icon(Icons.alternate_email),
    color: Colors.amber
      ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Text('click'),
        backgroundColor: Colors.amberAccent,
        onPressed: () {},
      ),
    );
  }
}


//ROUTES ---------------------------------------------------------------------
class FirstRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Route'),
      ),
      body: Center(
        child: ElevatedButton(
          child: Text('Open route'),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => SecondRoute()));
          },
        ),
      ),
    );
  }
}

class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Route"),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Go back!'),
        ),
      ),
    );
  }
}


